
//Connection to mongo ATLAS
mongo "mongodb://cluster0-shard-00-00-jxeqq.mongodb.net:27017,cluster0-shard-00-01-jxeqq.mongodb.net:27017,cluster0-shard-00-02-jxeqq.mongodb.net:27017/aggregations?replicaSet=Cluster0-shard-0" --authenticationDatabase admin --ssl -u m121 -p aggregations --norc

//Mongo Query 
1. show dbs
2. show collections
3. db.movies.findOne()

// Filtering documents with martch
db.solarSystem.aggregate([{
    $match: {}
}])

// find all documents which are not equal to the type : Star
1. db.solarSystem.aggregate([{
    $match: {type: {$ne: "Star"}}
}]).pretty()

// count all documents which are not equal to the type : Star
2. db.solarSystem.aggregate([{
    $match: {type: {$ne: "Star"}}
}, {$count: "planets"} ]).pretty()


// Find a movie using match
3. var pipeline = [ { $match: 
    { 
        "imdb.rating": {$gte: 7}, 
        "genres": {$nin: ["Crime", "Horroe"]}, 
        "rated": {$in: ["PG", "P"]}, 
       "languages": {$in: ["English", "Japanese"]} 
    } } ]

// Project is like Map
db.solarSystem.aggregate([{$project: {}  }])  

// query the solarSystem collection and remove the _id field and show the name field
4. db.solarSystem.aggregate([{$project: { _id: 0, name: 1}  }])  
//result => {"name" : "Mars" }

5. db.solarSystem.aggregate([{$project: { _id: 0, name: 1, "gravity.value": 1}  }])  
//result => { "name" : "Uranus", "gravity" : { "value" : 8.87 } }

//Reasign the value of gravity.value to gravity
6. db.solarSystem.aggregate([{$project: { _id: 0, name: 1, "gravity": "$gravity.value"}  }]) 
//result => { "name" : "Uranus", "gravity" : 8.87 }

// Rename the gravity field
7. db.solarSystem.aggregate([{$project: { _id: 0, name: 1, "surfaceGravity": "$gravity.value"}  }]) 
//result => { "name" : "Uranus", "surfaceGravity" : 8.87 }

// Calculate a value
8. db.solarSystem.aggregate([{$project: { _id: 0, name: 1, "miPeso": { 
                              $multiply: [ 
                                            { $divide: ["$gravity.value", 9.8] },
                                         68 
                                         ]
                                         }}  
                            }])

// find a movie using match and project
9. var pipeline = [
    { 
        $match:  { 
        "imdb.rating": {$gte: 7}, 
        "genres": {$nin: ["Crime", "Horroe"]}, 
        "rated": {$in: ["PG", "P"]}, 
       "languages": {$all: ["English", "Japanese"]} 
              } 
    }, 
    { $project: { _id: 0, "title": 1, "rated": 1} }]     
    
    // We begin with a $match stage, ensuring that we only allow movies where the title is a string
    // Next is our $project stage, splitting the title on spaces. This creates an array of strings
    // We use another $match stage to filter down to documents that only have one element in the newly computed title field, and use itcount() to get a count
   10. db.movies.aggregate([
        {
          $match: {
            title: {
              $type: "string"
            }
          }
        },
        {
          $project: {
            title: { $split: ["$title", " "] },
            _id: 0
          }
        },
        {
          $match: {
            title: { $size: 1 }
          }
        }
      ]).itcount()


      11. db.movies.find({}, {_id: 0, "writers": 1}).pretty()


      // There are times when we want to make sure that the field is an array, and that it is not empty. We can do this within $match
      12. db.movies.aggregate([
        { 
            $match: { writers: { $elemMatch: { $exists: true } } }
        }
      ]).pretty()


      // find a writer from the movies collection
      13. db.movies.findOne({title: "Life Is Beautiful"}, { _id: 0, cast: 1, writers: 1})
 /* Result => {
        "cast" : [
                "Roberto Benigni",
                " NicolettaBraschi",
                "Giustino Durano",
                "Giorgio Cantarini"
        ],
        "writers" : [
                "Vincenzo Cerami (story)",
                "Roberto Benigni (story)"
        ]
} */
 /*
 This presents a problem, since comparing "Roberto Benigni" to "Roberto Benigni (story)" will definitely result in a difference.
 Thankfully there is a powerful expression to help us, $map. $map lets us iterate over an array, element by element, 
 performing some transformation on each element. The result of that transformation will be returned in the same place as the original element.

 Within $map, the argument to input can be any expression as long as it resolves to an array. 
 The argument to as is the name of the variable we want to use to refer to each element of the array when performing whatever logic we want. 
 The field as is optional, and if omitted each element must be referred to as $$this:: 
 The argument to in is the expression that is applied to each element of the input array, referenced with the variable name specified in as, 
 and prepending two dollar signs: */
      14. db.movies.aggregate([
        { 
            $match: { writers: {
                $map: {
                  input: "$writers",
                  as: "writer",
                  in: {
                    $arrayElemAt: [
                      {
                        $split: [ "$$writer", " (" ]
                      },
                      0
                    ]
                  }
                }
              } }
        }
      ]).pretty()


      // With our first $match stage, we filter out documents that are not an array or have an empty array for the fields we are interested in.
      // Next is a $project stage, removing the _id field and retaining both the directors and cast fields. 
      // We replace the existing writers field with a new computed value, cleaning up the strings within writers
      // We use another $project stage to computer a new field called labor_of_love that ensures the intersection of cast, writers, 
      // and our newly cleaned directors is greater than 0. This definitely means that at least one element in each array is identical! $gt will return true or false.
      // Lastly, we follow with a $match stage, only allowing documents through where labor_of_love is true. In our example we use a $match stage, but itcount() works too.
      15. db.movies.aggregate([
        {
          $match: {
            cast: { $elemMatch: { $exists: true } },
            directors: { $elemMatch: { $exists: true } },
            writers: { $elemMatch: { $exists: true } }
          }
        },
        {
          $project: {
            _id: 0,
            cast: 1,
            directors: 1,
            writers: {
              $map: {
                input: "$writers",
                as: "writer",
                in: {
                  $arrayElemAt: [
                    {
                      $split: ["$$writer", " ("]
                    },
                    0
                  ]
                }
              }
            }
          }
        },
        {
          $project: {
            labor_of_love: {
              $gt: [
                { $size: { $setIntersection: ["$cast", "$directors", "$writers"] } },
                0
              ]
            }
          }
        },
        {
          $match: { labor_of_love: true }
        },
        {
          $count: "labors of love"
        }
      ])